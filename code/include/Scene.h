/*
  Author:   Pablo del Fresno Herena
  Date: 25/07/2019
*/

#pragma once

#include "GLSuite.h"

#include "Actor.h"

#include <unordered_map>
#include <vector>

class Scene {

  typedef std::shared_ptr<Scene> ScenePtr;

public:
  static ScenePtr s_currentScene;

private:
  size_t m_root_node;

public:
  Scene();

  /** Updates the complete scene graph. */
  void Update(float deltaTime);
  /** Draws the complete scene graph. */
  void Draw();

  ActorPtr GetRootNode();
  void AddActor(size_t actor);

};