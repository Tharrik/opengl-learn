/*
  Author:   Pablo del Fresno Herena
  Date: 25/07/2019
*/

#pragma once

#include "Transform.h"
#include "Component.h"

#include "third-party/shader.h"

#include <vector>
#include <list>

/** Returns the actor with the given ID. */
#define ACTOR(id) Actor::GetActorByID(id)

class Actor;
typedef std::shared_ptr<Actor> ActorPtr;

class Actor {

  typedef std::vector<std::shared_ptr<Actor>> ActorList;
  typedef std::vector<size_t> ComponentList;

  friend class Scene;

private:
  static ActorList s_actors;

public:
  /** Creates a new Actor and returns the id. */
  static ActorPtr Instantiate();
  static std::shared_ptr<Actor> GetActorByID(size_t actor_id);

private:
  static size_t AddActor(Actor* actor);

public:
  Transform m_transform{};

private:
  bool m_is_root;

  size_t m_id;
  size_t m_parent_id;
  std::list<size_t> m_children;

  ComponentList components;

public:
  Actor();

public:
  void Update(float deltaTime);
  void Draw(mat4 t_model_matrix);

  size_t GetID();

  void SetParent(size_t parent_id);
  std::shared_ptr<Actor> GetParent();

  void AddComponent(size_t component_id);

  /** Returns the first T component in this Actor. */
  template<class T>
  std::shared_ptr<T> GetComponent()
  {
    for (auto it = components.begin(); it != components.end(); ++it)
    {
      if (COMPONENT(*it)->GetType() == T::Type())
      {
        return std::dynamic_pointer_cast<T>(COMPONENT(*it));
      }
    }
    return std::shared_ptr<T>(nullptr);
  }

};
