/*
  Author:   Pablo del Fresno Herena
  Date: 25/07/2019
*/

#pragma once

#include "GLSuite.h"
#include "btBulletDynamicsCommon.h"
#include "LinearMath/btIDebugDraw.h"

#include "third-party/GLDebugDrawer.h"

class World {

public:
  static World& Instance();

public:
  std::unique_ptr<btDiscreteDynamicsWorld> m_world;

public:
  World(vec3 gravity = { 0.0f, -10.0f, 0.0f });

  /** The world ticks. */
  void Update(float deltaTime);

  void SetGravity(vec3 gravity);

  void AddRigidBody(btRigidBody& body);
  void AddCollisionObject(btCollisionObject* obj);

};
