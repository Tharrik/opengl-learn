#pragma once

#define _ENABLE_EXTENDED_ALIGNED_STORAGE

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

#include <memory>
#include <vector>

using namespace glm;
