/*
  Author:   Pablo del Fresno Herena
  Date: 25/07/2019
*/

#pragma once

#include "GLSuite.h"
#include "Actor.h"
#include "Component.h"
#include "Mesh.h"

#include "assimp/Importer.hpp"
#include "assimp/scene.h"
#include "assimp/postprocess.h"

#include <vector>
#include <string>

class Model : public Component
{
  typedef std::vector<size_t> MeshVec;
  typedef std::vector<size_t>::iterator MeshIt;

public:
  /** Returns de type id of this component class. */
  static size_t Type();

public:
  /** Bounding box calculated during mesh load. */
  vec3 m_bounding_size{};
  Shader& m_shader;

private:
  MeshVec m_meshes;
  vec3 AABBMax;
  vec3 AABBMin;

public:
  Model(size_t parent, const char* path, Shader& shader);
  
  virtual void Update(float deltaTime) override;
  virtual void Draw(mat4 model_matrix) override;

private:
  void LoadModel(std::string path);
  void ProcessMesh(aiMesh* mesh);

};
