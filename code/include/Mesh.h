/*
  Author:   Pablo del Fresno Herena
  Date: 25/07/2019
*/

#pragma once

#include "GLSuite.h"
#include "glm/gtc/matrix_transform.hpp"
#include "third-party/shader.h"

#include <string>
#include <vector>
#include <list>

using namespace glm;

struct Vertex {
  vec3 position;
  vec3 normal;
  vec2 textCoord;
};

struct Texture {
  GLuint id;
  std::string type;
  std::string path;
};

class Mesh {

  typedef std::vector<std::shared_ptr<Mesh>> MeshList;
  typedef std::shared_ptr<Mesh> MeshPtr;

  friend class Model;

private:
  static MeshList s_meshes;

public:
  static MeshPtr GetMeshByID(size_t mesh_id);

private:
  static size_t AddMesh(Mesh* mesh);

public:
  std::vector<Vertex> vertices;
  std::vector<GLuint> indices;
  std::vector<Texture> textures;
  GLuint VAO;

private:
  GLuint VBO;
  GLuint EBO;
  vec3 color{ 0.5f, 0.5f, 0.5f };

public:
  Mesh(std::vector<Vertex> vertices, std::vector<GLuint> indices);

  void Draw(Shader& shader, mat4 model);

private:
  void Setup();

};
