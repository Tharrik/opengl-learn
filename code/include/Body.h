/*
  Author:   Pablo del Fresno Herena
  Date: 25/07/2019
*/

#pragma once

#include "GLSuite.h"
#include "btBulletDynamicsCommon.h"
#include "Collider.h"
#include "Component.h"

#include <vector>

class Body : public Component
{
public:
  enum BodyType { Static, Dynamic, Kinematic };
  enum ForceType { Force, Impulse };

public:
  /** Returns de type id of this component class. */
  static size_t Type();

public:
  std::shared_ptr<btRigidBody> m_rigid_body;

private:
  btCollisionShape* m_collider;

  btTransform m_transform;
  btDefaultMotionState* m_motion_state;
  btScalar m_mass;
  btVector3 m_localInertia;

public:
  Body(size_t parent_id, Collider* collider, float mass = {0.0f});

  /** Updates the transform of its father. */
  void Update(float deltaTime) override;
  void Draw(mat4 model_matrix) override {}

  /** Moves the body with it's parent.
      You can't do this with the transform once there is a rigid body.
  */
  void Move(vec3 displacement);
  void Move(float x, float y, float z);
  void MoveTo(vec3 position);
  void MoveTo(float x, float y, float z);

  void SetRotation(quat rotation);

  float GetMass();

  /** Apply the given amount of given type of force. */
  void ApplyForce(vec3 force, ForceType type = Force);
  /** Stops the rigidbody */
  void Stop();

private:
  void SetMass(float mass);

};
