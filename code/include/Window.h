/*
  Author:   Pablo del Fresno Herena
  Date: 25/07/2019
*/

#pragma once
#define NOMINMAX

#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <string>

#include "GLSuite.h"
#include <glm/gtc/matrix_transform.hpp>

#include "third-party/shader.h"

#include "Camera.h"
#include "Input.h"
#include "Scene.h"
#include "Model.h"
#include "Light.h"
#include "OpenglScenario.h"
#include "BulletScenario.h"

class Window {

public:
  enum PostPro { NONE, SEPIA, BW };

private:
  static std::vector<Shader*> s_shaders;
  static Scenario* s_scenario;

public:
  static PostPro s_postpro;
  static vec4 s_background_color;
  static GLFWwindow* s_window;
  static Camera* s_camera;
  static Input s_input;
  static Light s_light;
  static double s_deltaTime;

public:
  /** Gets the window ready */
  static void Initialize();

  /** Starts the update-draw cycle */
  static void Start();

  /** Clean up method*/
  static void Terminate();

  static void Update();
  static void Render();

  static void SetPostpro(PostPro postpro);

  static Shader& GenerateShader(const char* vertexPath, const char* fragmentPath, const char* geometryPath = nullptr);
  static Shader& GetShader(size_t index);

private:
  static void UpdateDeltaTime();
  static void ApplyBGColor(vec4 background_color);

};
