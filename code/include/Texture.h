#pragma once

#include <stdio.h>
#include <iostream>

#include <GL/glew.h>
#define STB_IMAGE_IMPLEMENTATION
#include "third-party/stb/stb_image.h"

class Texture {

public:
  static GLuint LoadTexture(const char* textureFilePath);

};
