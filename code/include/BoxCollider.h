#pragma once

#include "Collider.h"

class BoxCollider : public Collider {

public:
  BoxCollider(vec3 size);
  BoxCollider(float x, float y, float z);

};
