/*
  Author:   Pablo del Fresno Herena
  Date: 25/07/2019
*/

#pragma once

#include "GL/glew.h"
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

#include <vector>
#include <unordered_map>

using namespace glm;

class Input {

  typedef std::unordered_map<int, bool> InputMap;
  typedef std::pair<int, bool> InputPair;

  GLFWwindow* window;
  vec2 m_mouseOffset;
  vec2 m_screenSize;

  InputMap m_inputs;

public:
  void Initialize();
  void Update(float deltaTime);

  /** Returns the offset of the mouse from the last tick. */
  vec2 GetMouseOffset();

  /** Returns the state of the given mouse state. */
  bool GetMouseState(int mouseInput);
  /** Returns the state of the given unput state. */
  bool GetInputState(int input);

private:
  void UpdateMousePosition();
  void UpdateInputs();

  void ResetCursor();
};
