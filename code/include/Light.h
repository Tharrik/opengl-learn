/*
  Author:   Pablo del Fresno Herena
  Date: 25/07/2019
*/

#pragma once

#include "GLSuite.h"
#include "Transform.h"

class Light {

public:
  vec3 m_position;

private:
  float m_ambient_intensity;
  float m_specular_intensity;
  vec3 m_color;
  vec3 m_ambient_color;

public:
  Light(vec3 position = { 0.0f, 0.0f, 0.0f }, vec3 color = { 1.0f, 1.0f, 1.0f }, float ambient_intensity = 0.1f, float specular_intensity = 0.5f);

  void SetColor(vec3 color);
  vec3 GetColor();

  // Ambient
  void SetAmbientIntensity(float intensity);
  float GetAmbientIntensity();
  vec3 GetAmbientColor();

  // Specular
  void SetSpecularIntensity(float intensity);
  float GetSpecularIntensity();

};
