/*
  Author:   Pablo del Fresno Herena
  Date: 26/07/2019
*/

#pragma once

#include "GLSuite.h"

#include "btBulletDynamicsCommon.h"
#include "BulletCollision/CollisionDispatch/btGhostObject.h"

#include "Collider.h"
#include "Body.h"
#include "Component.h"


class Trigger : public Component
{

public:
  /** Returns de type id of this component class. */
  static size_t Type();

private:
  btGhostObject m_ghost;
  std::unique_ptr<btTransform> m_transform;

  btCollisionShape* m_collider;

public:
  Trigger(size_t parent_id, Collider* collider);

  /** Checks if this trigger is colliding with the given body. */
  bool CheckCollision(Body& body);

  virtual void Update(float deltaTime) override {}
  virtual void Draw(mat4 model_matrix) override {}

};
