/*
  Author:   Pablo del Fresno Herena
  Date: 25/07/2019
*/

#pragma once

#include "GLSuite.h"
#include "glm/gtc/matrix_transform.hpp"

using namespace glm;

enum AngularUnit { Degrees, Radians };
enum CameraMovement { FORWARD, BACKWARD, LEFT, RIGHT };

class Camera{

public:
  static vec3 worldUp;

public:
  vec3 position;
  vec3 front{};
  vec3 up{};
  vec3 right{};
  float yaw = -90.0f;
  float pitch = 0.0f;
  float speed = 5.0f;
  float mouseSensitivity = 0.2f;
  float zoom = 30.0f;

protected:
  float m_fov = radians(45.0f);
  float m_aspectRatio = 1.6f;
  float m_nearDistance = 0.1f;
  float m_farDistance = 100.0f;

  mat4 m_projectionMat{};

public:
  Camera(vec3 position = { 0.0f, 0.0f, 0.0f });

public:
  void Update(float deltaTime);

  void SetFieldOfView(float angle, AngularUnit unit = Degrees);
  void SetAspectRatio(int width, int height);
  void SetNearDistance(float nearDistance);
  void SetFarDistance(float farDistance);

  mat4 GetProjectionMatrix();
  mat4 GetViewMatrix();

private:
  void UpdateProjectionMatrix();
  void UpdateCameraVectors();

};
