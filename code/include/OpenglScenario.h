#pragma once

#include "Scenario.h"
#include "Input.h"
#include "Actor.h"
#include "Model.h"

class OpenGLScenario : public Scenario {

  Input& m_input;
  std::shared_ptr<Shader> m_simple_shader;
  std::shared_ptr<Shader> m_noise_shader;

  size_t m_ship;
  size_t m_sea;
  float angle;

public:
  OpenGLScenario();

  // Setup
  virtual void Setup() override;
  void CreateShaders();

  // Update
  virtual void Update(float deltaTime) override;
  void CheckPostpro();

  // Draw
  virtual void Draw() override {}

};
