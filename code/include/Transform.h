/*
  Author:   Pablo del Fresno Herena
  Date: 25/07/2019
*/

#pragma once

#include "GLSuite.h"
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtc/matrix_transform.hpp>

class Transform {

protected:
  vec3 m_position;
  quat m_rotation;
  vec3 m_scale;

public:
  Transform(vec3 position = { 0.0f, 0.0f, 0.0f }, quat rotation = { 0.0f, 0.0f, 0.0f, 1.0f }, vec3 scale = { 1.0f, 1.0f, 1.0f });

  vec3 GetPosition();
  quat GetRotation();
  vec3 GetEulerAngles();
  vec3 GetScale();

  void Move(const vec3& movement);
  void Move(float x, float y, float z);
  void MoveTo(const vec3& position);
  void MoveTo(float x, float y, float z);

  void Rotate(const vec3& rotation);
  void Rotate(float x, float y, float z);
  void SetRotation(const quat& rotation);
  void SetEulerAngles(const vec3& angles);
  void SetEulerAngles(float x, float y, float z);

  void SetScale(const vec3& scale);
  void SetScale(float x, float y, float z);

  void Scale(float scale);
  void Scale(const vec3& scale);
  void Scale(float x, float y, float z);

  /** Returns the transform matrix. */
  mat4 GetModelMatrix();

};