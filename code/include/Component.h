/*
  Author:   Pablo del Fresno Herena
  Date: 25/07/2019
*/

#pragma once

#include <third-party/shader.h>

#define PARENT Actor::GetActorByID(m_parent_id)
#define COMPONENT(id) Component::GetComponentByID(id)

class Component {

  typedef std::vector<std::shared_ptr<Component>> ComponentList;
  typedef std::shared_ptr<Component> ComponentPtr;

protected:
  static ComponentList s_components;
  /** Used to generate component type ids */
  static size_t s_next_type;

public:
  static ComponentPtr GetComponentByID(size_t component_id);

protected:
  size_t m_id;
  size_t m_parent_id;
  size_t m_component_type = -1;

public:
  Component(size_t parent_id, size_t component_type);

  size_t GetID();
  /** Returns the type id of this component. */
  size_t GetType();

  virtual void Update(float deltaTime) = 0;
  virtual void Draw(mat4 model_matrix) = 0;

};
