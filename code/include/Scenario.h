/*
  Author:   Pablo del Fresno Herena
  Date: 25/07/2019
*/

#pragma once

class Scenario {

public:
  /** Here the user can setup the scene. Called only once. */
  virtual void Setup() = 0;

  /** Here the user can update the scene. */
  virtual void Update(float deltaTime) = 0;

  /** Here the user can prepare the scene to be drawn. */
  virtual void Draw() = 0;

};