/*
  Author:   Pablo del Fresno Herena
  Date: 25/07/2019
*/

#pragma once
#define _ENABLE_EXTENDED_ALIGNED_STORAGE

#include "Scenario.h"
#include "World.h"
#include "Actor.h"
#include "Model.h"
#include "Body.h"
#include "Trigger.h"
#include "BoxCollider.h"
#include "SphereCollider.h"

class BulletScenario : public Scenario
{
  size_t m_platform;
  std::shared_ptr<Body> m_platform_body;

  size_t m_platform_trigger;
  std::shared_ptr<Trigger> m_platform_trigger_c;
  bool is_platform_moving = false;

  size_t m_sphere;
  std::shared_ptr<Body> m_sphere_body;
  float m_sphere_mass = 10.0f;
  float m_sphere_impulse = 25.0f;

  size_t m_key;
  bool is_door_closed = true; 
  std::shared_ptr<Trigger> m_key_trigger;

  size_t m_door;
  std::shared_ptr<Body> m_door_body;

  std::shared_ptr<Shader> m_simple_shader;

public:
  virtual void Setup() override;
  virtual void Update(float deltaTime) override;
  virtual void Draw() override;

private:
  void Reset();

  void CreateStaticBodies();
  size_t CreateWall(vec3 position, float rotation, float size = 5.0f, float mass = 0.0f);
  size_t CreateWall(float x, float y, float z, float rotation, float size = 5.0f, float mass = 0.0f);
  size_t CreateColumn(vec3 position, float mass = 0.0f);
  size_t CreateColumn(float x, float y, float z, float mass = 0.0f);
  size_t CreateFloor(vec3 position, float length, float width, float mass = 0.0f);
  size_t CreateFloor(float x, float y, float z, float length, float width, float mass = 0.0f);

  void CreateBall();
  void UpdateBallInput(float delta_time);
  void CheckBallPosition();

  void CheckPlatform();
  void MovePlatform(float delta_time);

};