#version 330

layout(location = 0) in vec3 vPosition;
layout(location = 1) in vec3 vNormal;

uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;

out vec3 FragPos;
out vec3 Normal;

void main()
{
	FragPos = vec3(model * vec4(vPosition, 1.0));
	Normal = mat3(transpose(inverse(model))) * vNormal;

	gl_Position = projection * view * model * vec4(vPosition, 1.0);
}
