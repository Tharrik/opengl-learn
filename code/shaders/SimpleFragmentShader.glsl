#version 330

uniform vec3 lightPos;
uniform vec3 viewPos;
uniform vec3 objectColor;
uniform vec3 lightColor;

uniform vec3 ambient_light;
uniform float specular_intensity;

uniform int postpro;

in vec3 Normal;
in vec3 FragPos;

out vec4 FragColor;

void main()
{
    // diffuse 
    vec3 norm = normalize(Normal);
    vec3 lightDir = normalize(lightPos - FragPos);
    float diff = max(dot(norm, lightDir), 0.0);
    vec3 diffuse = diff * lightColor;
    
    // specular
    vec3 viewDir = normalize(viewPos - FragPos);
    vec3 reflectDir = reflect(-lightDir, norm);  
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), 32);
    vec3 specular = specular_intensity * spec * lightColor; 
        
    vec3 color = objectColor;

    if(postpro == 1)
    {
        // Sepia Postpro
        color.r = 0.393 * objectColor.r + 0.769 * objectColor.g + 0.189 * objectColor.b;
        color.g = 0.349 * objectColor.r + 0.686 * objectColor.g + 0.168 * objectColor.b;
        color.b = 0.272 * objectColor.r + 0.534 * objectColor.g + 0.131 * objectColor.b;
    }
    else if(postpro == 2)
    {
        // Black and white postpro
        float mean = (objectColor.r + objectColor.g + objectColor.b) * 0.33333f;
        color.r = mean;
        color.g = mean;
        color.b = mean;
    }

    vec3 result = (ambient_light + diffuse + specular) * color;
    FragColor = vec4(result, 1.0);
}
