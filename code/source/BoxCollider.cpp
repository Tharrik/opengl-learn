#include "BoxCollider.h"

BoxCollider::BoxCollider(vec3 size) :
  BoxCollider{size.x, size.y, size.z }
{}

BoxCollider::BoxCollider(float x, float y, float z)
{
  m_shape = new btBoxShape(btVector3{ x * 0.5f, y * 0.5f, z * 0.5f });
}
