#include "Input.h"
#include "Window.h"

void Input::Initialize()
{
  window = Window::s_window;

  GLint screenSize[4];
  glGetIntegerv(GL_VIEWPORT, screenSize);
  m_screenSize.x = screenSize[2];
  m_screenSize.y = screenSize[3];

  ResetCursor();
}

void Input::Update(float t_deltaTime)
{
  // MOUSE
  UpdateMousePosition();

  // KEYBOARD
  UpdateInputs();
}

vec2 Input::GetMouseOffset()
{
  return m_mouseOffset;
}

bool Input::GetMouseState(int mouseInput)
{
  return GetInputState(mouseInput);
}

bool Input::GetInputState(int input)
{
  InputMap::iterator it = m_inputs.find(input);
  if (it != m_inputs.end())
  {
    return it->second;
  }

  bool state = glfwGetKey(window, input) == GLFW_PRESS;
  InputPair newInput{ input, state };
  m_inputs.insert(newInput);

  return state;
}

void Input::UpdateMousePosition()
{
  double x, y;
  glfwGetCursorPos(window, &x, &y);
  m_mouseOffset.x = floor(x - (double)m_screenSize.x * 0.5f);
  m_mouseOffset.y = floor((double)m_screenSize.y * 0.5f - y);

  ResetCursor();
}

void Input::UpdateInputs()
{
  for (InputMap::iterator it = m_inputs.begin(); it != m_inputs.end(); ++it)
  {
    it->second = glfwGetKey(window, it->first) == GLFW_PRESS;
  }
}

void Input::ResetCursor()
{
  glfwSetCursorPos(window, m_screenSize.x * 0.5, m_screenSize.y * 0.5);
}
