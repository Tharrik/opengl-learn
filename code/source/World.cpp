#include "World.h"
#include "Window.h"

World& World::Instance()
{
  static std::unique_ptr<World> instance = std::make_unique<World>();
  return *instance;
}

World::World(vec3 t_gravity)
{
  btDefaultCollisionConfiguration* collisionConfiguration = new btDefaultCollisionConfiguration();
  btCollisionDispatcher* dispatcher = new btCollisionDispatcher(collisionConfiguration);
  btBroadphaseInterface* overlappingPairCache = new btDbvtBroadphase();
  btSequentialImpulseConstraintSolver* solver = new btSequentialImpulseConstraintSolver;

  m_world = std::make_unique<btDiscreteDynamicsWorld>(dispatcher, overlappingPairCache, solver, collisionConfiguration);
  SetGravity(t_gravity);
}

void World::Update(float t_deltaTime)
{
  m_world->stepSimulation(t_deltaTime, 10);
}

void World::SetGravity(vec3 t_gravity)
{
  m_world->setGravity(btVector3{ t_gravity.x, t_gravity.y, t_gravity.z });
}

void World::AddRigidBody(btRigidBody& t_body)
{
  Instance().m_world->addRigidBody(&t_body);
}

void World::AddCollisionObject(btCollisionObject* obj)
{
  Instance().m_world->addCollisionObject(obj);
}
