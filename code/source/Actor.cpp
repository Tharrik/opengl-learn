#include "Actor.h"
#include "Scene.h"

// STATIC

Actor::ActorList Actor::s_actors;

ActorPtr Actor::Instantiate()
{
  ActorPtr actor = std::make_shared<Actor>();

  s_actors.push_back(actor);

  actor->m_id = s_actors.size() - 1;
  actor->m_is_root = actor->m_id == 0;

  if (!actor->m_is_root)
  {
    Scene::s_currentScene->AddActor(actor->m_id);
  }

  return actor;
}

std::shared_ptr<Actor> Actor::GetActorByID(size_t t_actor_id)
{
  return s_actors[t_actor_id];
}

size_t Actor::AddActor(Actor* actor)
{
  s_actors.push_back(std::shared_ptr<Actor>(actor));

  return s_actors.size() - 1;
}

// NON STATIC

Actor::Actor() {}

void Actor::Update(float t_deltaTime)
{
  for (ComponentList::iterator it = components.begin(); it != components.end(); ++it)
  {
    COMPONENT(*it)->Update(t_deltaTime);
  }

  for (auto it = m_children.begin(); it != m_children.end(); ++it)
  {
    s_actors[*it]->Update(t_deltaTime);
  }
}

void Actor::Draw(mat4 t_model_matrix)
{
  mat4 model = t_model_matrix * m_transform.GetModelMatrix();
  for (ComponentList::iterator it = components.begin(); it != components.end(); ++it)
  {
    COMPONENT(*it)->Draw(model);
  }

  for (auto it = m_children.begin(); it != m_children.end(); ++it)
  {
    s_actors[*it]->Draw(model);
  }
}

size_t Actor::GetID()
{
  return m_id;
}

void Actor::SetParent(size_t t_parent_id)
{
  // The root node can't be manipulated.
  if (m_is_root)
    return;

  s_actors[m_parent_id]->m_children.remove(m_id);
  m_parent_id = t_parent_id;
  s_actors[m_parent_id]->m_children.push_back(m_id);
}

std::shared_ptr<Actor> Actor::GetParent()
{
  // The root node returns itself as parent. Failsafe.
  if (m_is_root)
    return s_actors[m_id];

  return s_actors[m_parent_id];
}

void Actor::AddComponent(size_t t_component_id)
{
  components.push_back(t_component_id);
}
