#include "Camera.h"
#include "Window.h"

vec3 Camera::worldUp{ 0.0f, 1.0f, 0.0f };

Camera::Camera(vec3 t_position):
  position{t_position}
{
  UpdateProjectionMatrix();
}

void Camera::Update(float t_deltaTime)
{
  Input& input = Window::s_input;

  float velocity = speed * t_deltaTime;

  if (input.GetInputState(GLFW_KEY_LEFT_SHIFT))
    velocity *= 5.0f;

  if (input.GetInputState(GLFW_KEY_W))
    position += front * velocity;
  else if (input.GetInputState(GLFW_KEY_S))
    position -= front * velocity;

  if (input.GetInputState(GLFW_KEY_D))
    position += right * velocity;
  else if(input.GetInputState(GLFW_KEY_A))
    position -= right * velocity;

  if (input.GetInputState(GLFW_KEY_SPACE))
    position += worldUp * velocity;
  else if (input.GetInputState(GLFW_KEY_LEFT_CONTROL))
    position -= worldUp * velocity;

  yaw += input.GetMouseOffset().x * mouseSensitivity;
  pitch += input.GetMouseOffset().y * mouseSensitivity;

  if (pitch > 89.0f)
    pitch = 89.0f;
  else if (pitch < -89.0f)
    pitch = -89.0f;

  UpdateCameraVectors();
}

void Camera::SetFieldOfView(float t_angle, AngularUnit t_unit)
{
  if (t_unit == Degrees)
  {
    m_fov = radians(t_angle);
  }
  else
  {
    m_fov = t_angle;
  }
  UpdateProjectionMatrix();
}

void Camera::SetAspectRatio(int t_width, int t_height)
{
  m_aspectRatio = (float)t_width / (float)t_height;
  UpdateProjectionMatrix();
}

void Camera::SetNearDistance(float t_nearDistance)
{
  if (m_nearDistance <= 0.0f)
    return;

  m_nearDistance = t_nearDistance;
  UpdateProjectionMatrix();
}

void Camera::SetFarDistance(float t_farDistance)
{
  if (t_farDistance <= 0.0f)
    return;

  m_farDistance = t_farDistance;
  UpdateProjectionMatrix();
}

mat4 Camera::GetProjectionMatrix()
{
  return m_projectionMat;
}

mat4 Camera::GetViewMatrix()
{
  return lookAt(position, position + front, up);
}

void Camera::UpdateProjectionMatrix()
{
  m_projectionMat = perspective(m_fov, m_aspectRatio, m_nearDistance, m_farDistance);
}

void Camera::UpdateCameraVectors()
{
  vec3 aux{};
  aux.x = cos(radians(yaw)) * cos(radians(pitch));
  aux.y = sin(radians(pitch));
  aux.z = sin(radians(yaw)) * cos(radians(pitch));
  front = normalize(aux);

  right = normalize(glm::cross(front, worldUp));
  up = glm::normalize(glm::cross(right, front));

}
