#include "Body.h"
#include "Actor.h"
#include "World.h"

size_t Body::Type()
{
  static size_t type_id = Component::s_next_type++;
  return type_id;
}

Body::Body(size_t t_parent_id, Collider* collider, float t_mass) :
  Component{t_parent_id, Type()}
{
  m_collider = collider->m_shape;

  std::shared_ptr<Actor> parent = PARENT;
  vec3 scale = parent->m_transform.GetScale();
  m_collider->setLocalScaling(btVector3{ scale.x, scale.y, scale.z });

  m_localInertia = {0.0f, 0.0f, 0.0f};
  SetMass(t_mass);

  m_transform.setIdentity();
  vec3 position = parent->m_transform.GetPosition();
  quat rotation = parent->m_transform.GetRotation();

  m_transform.setOrigin(btVector3{
    position.x,
    position.y,
    position.z});
  
  m_transform.setRotation(btQuaternion{
    rotation.x,
    rotation.y,
    rotation.z,
    rotation.w });

  m_motion_state = new btDefaultMotionState(m_transform);
  btRigidBody::btRigidBodyConstructionInfo rbInfo(m_mass, m_motion_state, m_collider, m_localInertia);
  m_rigid_body = std::make_shared<btRigidBody>(rbInfo);

  m_rigid_body->setUserIndex(m_id);

  //add the body to the dynamics world
  World::Instance().AddRigidBody(*m_rigid_body);
}

void Body::Update(float t_deltaTime)
{
  m_rigid_body->getMotionState()->getWorldTransform(m_transform);

  btVector3 position = m_transform.getOrigin();
  btQuaternion rotation = m_transform.getRotation();

  // std::cout << position.x() << " " << position.y() << " " << position.z() << std::endl;

  std::shared_ptr<Actor> parent = PARENT;
  parent->m_transform.MoveTo(vec3{
    position.x(),
    position.y(),
    position.z() });

  parent->m_transform.SetRotation(quat{
    rotation.w(),
    rotation.x(),
    rotation.y(), 
    rotation.z() });
}

void Body::Move(vec3 t_displacement)
{
  MoveTo( ACTOR(m_parent_id)->m_transform.GetPosition() + t_displacement);
}

void Body::Move(float t_x, float t_y, float t_z)
{
  Move(vec3{ t_x, t_y, t_z });
}

void Body::MoveTo(vec3 t_position)
{
  MoveTo( t_position.x, t_position.y, t_position.z );
}

void Body::MoveTo(float t_x, float t_y, float t_z)
{
  Actor::GetActorByID(m_parent_id)->m_transform.MoveTo(vec3{ t_x, t_y, t_z });
  m_transform.setOrigin(btVector3{ t_x, t_y, t_z });

  m_rigid_body->setWorldTransform(m_transform);
  m_motion_state->setWorldTransform(m_transform);
}

void Body::SetRotation(quat rotation)
{
  PARENT->m_transform.SetRotation(rotation);

  m_transform.setRotation(btQuaternion{
    rotation.x,
    rotation.y,
    rotation.z,
    rotation.w });
}

void Body::SetMass(float t_mass)
{
  if (t_mass < 0.0f)
    return;

  m_mass = t_mass;
  
  if (m_mass > 0.0f)
    m_collider->calculateLocalInertia(m_mass, m_localInertia);
}

float Body::GetMass()
{
  return m_mass;
}

void Body::ApplyForce(vec3 force, ForceType type)
{
  btVector3 aux{ force.x, force.y, force.z };
  if (type == Force)
  {
    m_rigid_body->applyCentralForce(aux);
  }
  else
  {
    m_rigid_body->applyCentralImpulse(aux);
  }
}

void Body::Stop()
{
  // The linear velocity cannot be zero, that freezes the rigidbody.
  m_rigid_body->setLinearVelocity(btVector3(0.0f, -0.1f, 0.0f));
}
