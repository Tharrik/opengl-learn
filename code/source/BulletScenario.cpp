#include "..\include\BulletScenario.h"
#include "Window.h"

void BulletScenario::Reset()
{
  // Reset ball
  m_sphere_body->MoveTo(0.0f, 5.0f, 0.0f);
  m_sphere_body->Stop();

  // Reset door
  is_door_closed = true;
  std::shared_ptr<Model> model = ACTOR(m_door)->GetComponent<Model>();
  ACTOR(m_door)->GetComponent<Body>()->MoveTo(-7.5f, 0.0f + model->m_bounding_size.y, 0.0f);

  // Reset platform
  m_platform_body->MoveTo(-15.0f, -0.5f, 0.0f);
  is_platform_moving = false;
}

void BulletScenario::CreateStaticBodies()
{
  CreateFloor(5.0f, -0.5f, 0.0f, 30.0f, 20.0f);
  CreateFloor(-60.0f, -0.5f, 0.0f, 20.0f, 60.0f);

  CreateColumn(-7.5f, 0.0f, 7.5f);
  CreateColumn(-7.5f, 0.0f, -7.5f);
  CreateColumn(17.5f, 0.0f, 7.5f);
  CreateColumn(17.5f, 0.0f, -7.5f);

  CreateWall(17.5f, 0.0f, 0.0f, 0.0f, 15.0f);
  CreateWall(5.0f, 0.0f, 7.5f, pi<float>() * 0.5f, 25.0f);
  CreateWall(5.0f, 0.0f, -7.5f, pi<float>() * 0.5f, 25.0f);

  for (float f = -27.5f; f < 27.55f; f += 2.5f)
  {
    CreateColumn(-65.0f, 0.0f, f, 1.0f);
  }

}

size_t BulletScenario::CreateWall(vec3 t_position, float t_rotation, float t_length, float t_mass)
{
  return CreateWall(t_position.x, t_position.y, t_position.z, t_rotation, t_length, t_mass);
}

size_t BulletScenario::CreateWall(float t_x, float t_y, float t_z, float t_rotation, float t_length, float t_mass)
{
  t_length *= 0.1f;

  ActorPtr wall = Actor::Instantiate();
  Model* model = new Model(wall->GetID(), "../../assets/models/Wall_5m.FBX", *m_simple_shader);

  wall->m_transform.MoveTo(t_x, t_y + model->m_bounding_size.y, t_z);
  wall->m_transform.Rotate(0.0f, t_rotation, 0.0f);
  wall->m_transform.Scale(2.0f);
  wall->m_transform.Scale(1.0f, 1.0f, t_length);
  new Body(wall->GetID(), new BoxCollider(model->m_bounding_size), t_mass);

  return wall->GetID();
}

size_t BulletScenario::CreateColumn(vec3 t_position, float t_mass)
{
  return CreateColumn(t_position.x, t_position.y, t_position.z, t_mass);
}

size_t BulletScenario::CreateColumn(float t_x, float t_y, float t_z, float t_mass)
{
  ActorPtr column = Actor::Instantiate();
  Model* model = new Model(column->GetID(), "../../assets/models/Wall_Column.FBX", *m_simple_shader);

  column->m_transform.MoveTo(t_x, t_y + model->m_bounding_size.y, t_z);
  column->m_transform.Scale(2.0f);
  new Body(column->GetID(), new BoxCollider(model->m_bounding_size), t_mass);

  return column->GetID();
}

size_t BulletScenario::CreateFloor(vec3 t_position, float t_length, float t_width, float t_mass)
{
  return CreateFloor(t_position.x, t_position.y, t_position.z, t_length, t_width, t_mass);
}

size_t BulletScenario::CreateFloor(float t_x, float t_y, float t_z, float t_length, float t_width, float t_mass)
{
  ActorPtr floor = Actor::Instantiate();
  floor->m_transform.Scale(t_length, 1.0f, t_width);
  floor->m_transform.MoveTo(t_x, t_y, t_z);
  new Model(floor->GetID(), "../../assets/models/Box.FBX", *m_simple_shader);
  new Body(floor->GetID(), new BoxCollider(1.0f, 1.0f, 1.0f), t_mass);

  return floor->GetID();
}

void BulletScenario::CreateBall()
{
  m_sphere = Actor::Instantiate()->GetID();
  ACTOR(m_sphere)->m_transform.MoveTo(0.0f, 5.0f, 0.0f);
  new Model(m_sphere, "../../assets/models/CoolSphere.FBX", *m_simple_shader);
  SphereCollider* sphereCollider = new SphereCollider(1.0f);
  new Body(m_sphere, sphereCollider, m_sphere_mass);
  m_sphere_body = ACTOR(m_sphere)->GetComponent<Body>();
  m_sphere_body->m_rigid_body->setActivationState(DISABLE_DEACTIVATION);
}

void BulletScenario::UpdateBallInput(float t_delta_time)
{
  if (Window::s_input.GetInputState(GLFW_KEY_UP))
  {
    m_sphere_body->ApplyForce({ m_sphere_impulse * t_delta_time, 0.0f, 0.0f }, Body::ForceType::Impulse);
  }

  if (Window::s_input.GetInputState(GLFW_KEY_DOWN))
  {
    m_sphere_body->ApplyForce({ -m_sphere_impulse * t_delta_time, 0.0f, 0.0f }, Body::ForceType::Impulse);
  }

  if (Window::s_input.GetInputState(GLFW_KEY_LEFT))
  {
    m_sphere_body->ApplyForce({ 0.0f, 0.0f, -m_sphere_impulse * t_delta_time }, Body::ForceType::Impulse);
  }

  if (Window::s_input.GetInputState(GLFW_KEY_RIGHT))
  {
    m_sphere_body->ApplyForce({ 0.0f, 0.0f, m_sphere_impulse * t_delta_time }, Body::ForceType::Impulse);
  }
}

void BulletScenario::CheckBallPosition()
{
  if (Window::s_input.GetInputState(GLFW_KEY_R) || ACTOR(m_sphere)->m_transform.GetPosition().y < -10.0f)
  {
    Reset();
  }
}

void BulletScenario::CheckPlatform()
{
  if (!is_platform_moving && m_platform_trigger_c->CheckCollision(*m_sphere_body))
  {
    is_platform_moving = true;
  }
}

void BulletScenario::MovePlatform(float t_delta_time)
{
  if (is_platform_moving && ACTOR(m_platform)->m_transform.GetPosition().x > -45.0f)
  {
    m_platform_body->Move(-3.0f * t_delta_time, 0.0f, 0.0f);
  }
}

void BulletScenario::Setup()
{
  Window::s_camera->position = { -10.0f, 10.0f, 0.0f };
  Window::s_light.m_position = { 10, 20, 12 };

  m_simple_shader.reset(&Window::GenerateShader("../../code/shaders/SimpleVertexShader.glsl", "../../code/shaders/SimpleFragmentShader.glsl"));

  CreateStaticBodies();

  CreateBall();

  m_door = CreateWall(-7.5f, 0.0f, 0.0f, 0.0f, 15.0f);

  m_platform = Actor::Instantiate()->GetID();
  ACTOR(m_platform)->m_transform.SetScale(10.0f, 1.0f, 10.0f);
  ACTOR(m_platform)->m_transform.MoveTo(-15.0f, -0.5f, 0.0f);
  Model* platform_model = new Model(m_platform, "../../assets/models/Box.FBX", *m_simple_shader);
  BoxCollider* platform_collider = new BoxCollider(platform_model->m_bounding_size);
  new Body(m_platform, platform_collider, 0.0f);
  m_platform_body = ACTOR(m_platform)->GetComponent<Body>();

  m_platform_trigger = Actor::Instantiate()->GetID();
  ACTOR(m_platform_trigger)->m_transform.SetScale(5.0f, 1.0f, 5.0f);
  ACTOR(m_platform_trigger)->m_transform.MoveTo(-15.0f, 0.0f, 0.0f);
  platform_collider = new BoxCollider(platform_model->m_bounding_size * 0.5f);
  new Trigger(m_platform_trigger, platform_collider);
  m_platform_trigger_c = ACTOR(m_platform_trigger)->GetComponent<Trigger>();

  m_key = Actor::Instantiate()->GetID();
  ACTOR(m_key)->m_transform.Scale(0.025f);
  ACTOR(m_key)->m_transform.MoveTo(15.5f, 1.0f, 0.0f);
  Model* key_model = new Model(m_key, "../../assets/models/Key.FBX", *m_simple_shader);
  BoxCollider* box_collider = new BoxCollider(key_model->m_bounding_size);
  new Trigger(m_key, box_collider);
  m_key_trigger = ACTOR(m_key)->GetComponent<Trigger>();

}

void BulletScenario::Update(float t_delta_time)
{
  World::Instance().Update(t_delta_time);

  // Update ball input
  UpdateBallInput(t_delta_time);

  // Check ball for falls
  CheckBallPosition();

  // Check platform
  CheckPlatform();

  // Move Platform
  MovePlatform(t_delta_time);

  // Check key trigger
  if (is_door_closed && m_key_trigger->CheckCollision(*m_sphere_body))
  {
    std::shared_ptr<Model> model = ACTOR(m_door)->GetComponent<Model>();
    ACTOR(m_door)->GetComponent<Body>()->MoveTo(-7.5f, -10.0f + model->m_bounding_size.y, 0.0f);
    is_door_closed = false;
  }

}

void BulletScenario::Draw()
{

}
