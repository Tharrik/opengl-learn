#include "Transform.h"

#include <iostream>

Transform::Transform(vec3 t_position, quat t_rotation, vec3 t_scale) :
  m_position{ t_position },
  m_rotation{ t_rotation },
  m_scale{ t_scale }
{}

vec3 Transform::GetPosition()
{
  return m_position;
}

quat Transform::GetRotation()
{
  return m_rotation;
}

vec3 Transform::GetEulerAngles()
{
  return eulerAngles(m_rotation);
}

vec3 Transform::GetScale()
{
  return m_scale;
}

void Transform::Move(const vec3& t_movement)
{
  m_position += t_movement;
}

void Transform::Move(float t_x, float t_y, float t_z)
{
  m_position.x += t_x;
  m_position.y += t_y;
  m_position.z += t_z;
}

void Transform::MoveTo(const vec3& t_position)
{
  m_position = t_position;
}

void Transform::MoveTo(float t_x, float t_y, float t_z)
{
  m_position.x = t_x;
  m_position.y = t_y;
  m_position.z = t_z;
}

void Transform::Rotate(const vec3& t_rotation)
{
  quat aux = quat(t_rotation);
  m_rotation *= aux;
}

void Transform::Rotate(float t_x, float t_y, float t_z)
{
  Rotate(vec3{ t_x, t_y, t_z });
}

void Transform::SetRotation(const quat& rotation)
{
  m_rotation = rotation;
}

void Transform::SetEulerAngles(const vec3& t_angles)
{
  m_rotation = quat(t_angles);
}

void Transform::SetEulerAngles(float x, float y, float z)
{
  m_rotation = quat{ vec3{x, y, z} };
}

void Transform::SetScale(float t_x, float t_y, float t_z)
{
  SetScale(vec3{ t_x, t_y, t_z });
}

void Transform::SetScale(const vec3& t_scale)
{
  m_scale = t_scale;
}

void Transform::Scale(float t_scale)
{
  m_scale *= t_scale;
}

void Transform::Scale(const vec3& t_scale)
{
  Scale(t_scale.x, t_scale.y, t_scale.z);
}

void Transform::Scale(float t_x, float t_y, float t_z)
{
  m_scale.x *= t_x;
  m_scale.y *= t_y;
  m_scale.z *= t_z;
}

mat4 Transform::GetModelMatrix()
{
  mat4 model = mat4{ 1.0f };
  model = translate(model, m_position);
  model *= toMat4(m_rotation);
  model = scale(model, m_scale);

  return model;
}
