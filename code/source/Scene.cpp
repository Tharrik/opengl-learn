#include "Scene.h"

Scene::ScenePtr Scene::s_currentScene;

Scene::Scene()
{
  s_currentScene.reset(this);

  ActorPtr root_node = Actor::Instantiate();
  m_root_node = root_node->GetID();
}

void Scene::Update(float t_deltaTime)
{
  Actor::GetActorByID(m_root_node)->Update(t_deltaTime);
}

void Scene::Draw()
{
  Actor::GetActorByID(m_root_node)->Draw(Transform{}.GetModelMatrix());
}

std::shared_ptr<Actor> Scene::GetRootNode()
{
  return std::shared_ptr<Actor>();
}

void Scene::AddActor(size_t t_actor)
{
  ACTOR(m_root_node)->m_children.push_back(t_actor);
  ACTOR(t_actor)->m_parent_id = m_root_node;
}
