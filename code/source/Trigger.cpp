#include "Trigger.h"
#include "Actor.h"
#include "World.h"


size_t Trigger::Type()
{
  static size_t type_id = Component::s_next_type++;
  return type_id;
}

Trigger::Trigger(size_t t_parent_id, Collider* t_collider):
  Component{t_parent_id, Type()}
{
  m_collider = t_collider->m_shape;

  std::shared_ptr<Actor> parent = PARENT;
  vec3 scale = parent->m_transform.GetScale();
  m_collider->setLocalScaling(btVector3{ scale.x, scale.y, scale.z });

  vec3 position = parent->m_transform.GetPosition();
  quat rotation = parent->m_transform.GetRotation();
  m_transform = std::make_unique<btTransform>(
    btQuaternion{
      rotation.x,
      rotation.y,
      rotation.z,
      rotation.w },
    btVector3{
      position.x,
      position.y,
      position.z });

  m_ghost.setCollisionShape(m_collider);
  m_ghost.setWorldTransform(*m_transform);

  m_ghost.setCollisionFlags(4);

  World::Instance().AddCollisionObject(&m_ghost);
  World::Instance().m_world->getBroadphase()->getOverlappingPairCache()->setInternalGhostPairCallback(new btGhostPairCallback());
}

bool Trigger::CheckCollision(Body& t_body)
{
  for (size_t i = 0; i < m_ghost.getNumOverlappingObjects(); ++i)
  {
    btCollisionObject* collisionObj = m_ghost.getOverlappingObject(i);
    if (collisionObj->getUserIndex() == t_body.GetID())
      return true;
  }
  return false;
}
