#include "..\include\OpenglScenario.h"
#include "Window.h"

OpenGLScenario::OpenGLScenario():
  m_input{ Window::s_input }
{}

void OpenGLScenario::Setup()
{
  Window::s_camera->position = { 4, 3, 3 };
  Window::s_light.m_position = { 10, 20, 12 };

  CreateShaders();

  m_ship = Actor::Instantiate()->GetID();
  ACTOR(m_ship)->m_transform.Scale(0.1f);
  Model* model1 = new Model(m_ship, "../../assets/models/Ship_free.FBX", *m_simple_shader);

  m_sea = Actor::Instantiate()->GetID();
  Model* model2 = new Model(m_sea, "../../assets/models/Plane.FBX", *m_noise_shader);
}

void OpenGLScenario::CreateShaders()
{
  m_simple_shader.reset(&Window::GenerateShader("../../code/shaders/SimpleVertexShader.glsl", "../../code/shaders/SimpleFragmentShader.glsl"));
  m_noise_shader.reset(&Window::GenerateShader("../../code/shaders/NoiseVertexShader.glsl", "../../code/shaders/SimpleFragmentShader.glsl"));
}

void OpenGLScenario::Update(float t_deltaTime)
{
  angle += 1.0f * Window::s_deltaTime;
  ACTOR(m_ship)->m_transform.MoveTo(0.0f, 0.15f + 0.1f * cos(glfwGetTime()), 0.0f);

  CheckPostpro();
}

void OpenGLScenario::CheckPostpro()
{
  if (m_input.GetInputState(GLFW_KEY_R))
  {
    Window::SetPostpro(Window::PostPro::NONE);
  }
  else if (m_input.GetInputState(GLFW_KEY_1))
  {
    Window::SetPostpro(Window::PostPro::SEPIA);
  }
  else if (m_input.GetInputState(GLFW_KEY_2))
  {
    Window::SetPostpro(Window::PostPro::BW);
  }
}
