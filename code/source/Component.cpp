#include "Component.h"
#include "Actor.h"

// STATIC
Component::ComponentList Component::s_components;
size_t Component::s_next_type = 0;

Component::ComponentPtr Component::GetComponentByID(size_t component_id)
{
  return s_components[component_id];
}

// NON STATIC

Component::Component(size_t t_parent, size_t t_component_type):
  m_component_type{t_component_type}
{
  m_id = s_components.size();
  s_components.push_back(std::shared_ptr<Component>(this));

  m_parent_id = t_parent;
  PARENT->AddComponent(m_id);
}

size_t Component::GetID()
{
  return m_id;
}

size_t Component::GetType()
{
  return m_component_type;
}
