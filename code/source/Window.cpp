#include "Window.h"

GLFWwindow* Window::s_window;
Camera* Window::s_camera;
Input Window::s_input{};
double Window::s_deltaTime;

std::vector<Shader*> Window::s_shaders;
Window::PostPro Window::s_postpro = Window::PostPro::NONE;
vec4 Window::s_background_color{ 0.0f, 0.0f, 0.2f, 1.0f };

Light Window::s_light{ vec3{ 1.0f, 1.0f, 1.0f } };

Scenario* Window::s_scenario;

void Window::Initialize()
{
  // Initialize GLFW
  if (!glfwInit())
  {
    fprintf(stderr, "GLFW initialization failed!\n");
  }

  // Multisampling and antialiassing post-processing
  glfwWindowHint(GLFW_SAMPLES, 4);
  glEnable(GL_MULTISAMPLE);

  // Setup OpenGL
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

  s_window = glfwCreateWindow(1024, 768, "OpenGL Learn", NULL, NULL);
  if (s_window == NULL) {
    fprintf(stderr, "Failed to create a GLFW Window.\n");
    glfwTerminate();
    return;
  }
  glfwMakeContextCurrent(s_window);
  glewExperimental = true;
  if (glewInit() != GLEW_OK) {
    fprintf(stderr, "GLEW initialization failed!\n");
    return;
  }

  glfwSetInputMode(s_window, GLFW_STICKY_KEYS, GL_TRUE);
  glfwSetInputMode(s_window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);

  glClearColor(0.0f, 0.0f, 0.2f, 0.0f);

  //glEnable(GL_CULL_FACE);
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LESS);

  

  new Scene();

  s_camera = new Camera();

  s_input.Initialize();

  s_scenario = new OpenGLScenario();
  s_scenario->Setup();
}

void Window::Start()
{
  do
  {
    Update();
    Render();
  } while (
    !s_input.GetInputState(GLFW_KEY_ESCAPE) &&
    glfwWindowShouldClose(s_window) == 0);

  Terminate();
}

void Window::Terminate()
{
  // Cleanup VBO

  // Close OpenGL window and terminate GLFW
  glfwTerminate();
}

void Window::Update()
{
  UpdateDeltaTime();

  s_input.Update(s_deltaTime);
  s_camera->Update(s_deltaTime);
  Scene::s_currentScene->Update(s_deltaTime);

  s_scenario->Update(s_deltaTime);
}

void Window::Render()
{
  // Clear Screen
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  Scene::s_currentScene->Draw();
  s_scenario->Draw();

  glfwSwapBuffers(s_window);
  glfwPollEvents();
}

void Window::SetPostpro(PostPro postpro)
{
  s_postpro = postpro;

  vec4 color = s_background_color;
  if (s_postpro == PostPro::SEPIA)
  {
    color.r = 0.393 * s_background_color.r + 0.769 * s_background_color.g + 0.189 * s_background_color.b;
    color.g = 0.349 * s_background_color.r + 0.686 * s_background_color.g + 0.168 * s_background_color.b;
    color.b = 0.272 * s_background_color.r + 0.534 * s_background_color.g + 0.131 * s_background_color.b;
  }
  else if (s_postpro == PostPro::BW)
  {
    float mean = (s_background_color.r + s_background_color.g + s_background_color.b) * 0.33333f;
    color.r = mean;
    color.g = mean;
    color.b = mean;
  }

  ApplyBGColor(color);
}

Shader& Window::GenerateShader(const char* vertexPath, const char* fragmentPath, const char* geometryPath)
{
  s_shaders.push_back(new Shader(vertexPath, fragmentPath, geometryPath));

  return *s_shaders[s_shaders.size() - 1];
}

Shader& Window::GetShader(size_t t_index)
{
  return *s_shaders[t_index];
}

void Window::UpdateDeltaTime()
{
  static double lastTime = glfwGetTime();
  double currentTime = glfwGetTime();

  s_deltaTime = currentTime - lastTime;
  
  lastTime = currentTime;
}

void Window::ApplyBGColor(vec4 background_color)
{
  glClearColor(
    background_color.r,
    background_color.g,
    background_color.b,
    background_color.a
  );
}
