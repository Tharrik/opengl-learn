#include "Mesh.h"
#include "Scene.h"
#include "Window.h"

// STATIC

Mesh::MeshList Mesh::s_meshes;

Mesh::MeshPtr Mesh::GetMeshByID(size_t t_mesh_id)
{
  return s_meshes[t_mesh_id];
}

size_t Mesh::AddMesh(Mesh* t_mesh)
{
  s_meshes.push_back(std::shared_ptr<Mesh>(t_mesh));

  return s_meshes.size() - 1;
}

// NON STATIC

Mesh::Mesh(std::vector<Vertex> t_vertices, std::vector<GLuint> t_indices)
{
  vertices = t_vertices;
  indices = t_indices;
  // textures = t_textures;

  Setup();
}

void Mesh::Draw(Shader& t_shader, mat4 t_model)
{
  t_shader.use();

  t_shader.setMat4("projection", Window::s_camera->GetProjectionMatrix());
  t_shader.setMat4("view", Window::s_camera->GetViewMatrix());
  t_shader.setMat4("model", t_model);

  t_shader.setVec3("objectColor", color);

  t_shader.setVec3("lightColor", Window::s_light.GetColor());
  t_shader.setVec3("lightPos", Window::s_light.m_position);
  t_shader.setVec3("ambient_light", Window::s_light.GetAmbientColor());
  t_shader.setFloat("specular_intensity", Window::s_light.GetSpecularIntensity());

  t_shader.setVec3("viewPos", Window::s_camera->position);

  t_shader.setFloat("time", glfwGetTime());
  t_shader.setFloat("dim", 0.5f);

  t_shader.setInt("postpro", Window::s_postpro);

  glBindVertexArray(VAO);
  glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0);
  glBindVertexArray(0);
}

void Mesh::Setup()
{
  // create buffers/arrays
  glGenVertexArrays(1, &VAO);
  glGenBuffers(1, &VBO);
  glGenBuffers(1, &EBO);

  glBindVertexArray(VAO);

  // load data into vertex buffers
  glBindBuffer(GL_ARRAY_BUFFER, VBO);
  
  glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), &vertices[0], GL_STATIC_DRAW);

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), &indices[0], GL_STATIC_DRAW);

  // set the vertex attribute pointers
  // vertex Positions
  glEnableVertexAttribArray(0);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0);
  // vertex normals
  glEnableVertexAttribArray(1);
  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, normal));
  // vertex texture coords
  glEnableVertexAttribArray(2);
  glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, textCoord));

  glBindVertexArray(0);
}
