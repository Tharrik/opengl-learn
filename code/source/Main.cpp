#include <stdio.h>
#include <stdlib.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

#include "Window.h"

int main() {

  Window::Initialize();
  Window::Start();

  return 0;
}
