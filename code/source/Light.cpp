#include "Light.h"

Light::Light(vec3 t_position, vec3 t_color, float t_ambient_intensity, float t_specular_intensity) :
  m_position{ t_position },
  m_color{ t_color },
  m_ambient_intensity{ t_ambient_intensity },
  m_specular_intensity{ t_specular_intensity }
{
  m_ambient_color = m_ambient_intensity * m_color;
}

void Light::SetColor(vec3 t_color)
{
  if (t_color.x < 0.0f || t_color.y < 0.0f || t_color.z < 0.0f)
    return;

  m_color = t_color;
}

vec3 Light::GetColor()
{
  return m_color;
}

void Light::SetAmbientIntensity(float t_intensity)
{
  if (t_intensity < 0.0f || t_intensity > 1.0f)
    return;

  m_ambient_intensity = t_intensity;
  m_ambient_color = t_intensity * m_color;
}

float Light::GetAmbientIntensity()
{
  return m_ambient_intensity;
}

vec3 Light::GetAmbientColor()
{
  return m_ambient_color;
}

void Light::SetSpecularIntensity(float t_intensity)
{
  m_specular_intensity = max(t_intensity, 0.0f);
}

float Light::GetSpecularIntensity()
{
  return m_specular_intensity;
}
