#include "Model.h"

// STATIC
size_t Model::Type()
{
  static size_t type_id = Component::s_next_type++;
  return type_id;
}

// NON STATIC

Model::Model(size_t t_parent, const char* t_path, Shader& t_shader):
  Component{ t_parent, Type() },
  m_shader { t_shader }
{
  LoadModel(std::string(t_path));
}

void Model::LoadModel(std::string t_path)
{
  Assimp::Importer importer;
  const aiScene* scene = importer.ReadFile(t_path,
    aiProcess_Triangulate | aiProcess_FlipUVs | aiProcess_GenBoundingBoxes);

  if (!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode)
  {
    std::cout << "ERROR::ASSIMP::" << importer.GetErrorString() << std::endl;
    return;
  }

  for (size_t i = 0; i < scene->mNumMeshes; ++i)
  {
    ProcessMesh(scene->mMeshes[i]);
  }

  m_bounding_size.x = AABBMax.x - AABBMin.x;
  m_bounding_size.y = AABBMax.y - AABBMin.y;
  m_bounding_size.z = AABBMax.z - AABBMin.z;

}

void Model::ProcessMesh(aiMesh* t_mesh)
{
  std::vector<Vertex> vertices;
  std::vector<GLuint> indices;
  std::vector<Texture> textures;

  for (size_t i = 0; i < t_mesh->mNumVertices; ++i)
  {
    Vertex vertex;

    vertex.position.x = t_mesh->mVertices[i].x;
    vertex.position.y = t_mesh->mVertices[i].y;
    vertex.position.z = t_mesh->mVertices[i].z;

    vertex.normal.x = t_mesh->mNormals[i].x;
    vertex.normal.y = t_mesh->mNormals[i].y;
    vertex.normal.z = t_mesh->mNormals[i].z;

    if (t_mesh->mTextureCoords[0])
    {
      vertex.textCoord.x = t_mesh->mTextureCoords[0][i].x;
      vertex.textCoord.y = t_mesh->mTextureCoords[0][i].y;
    }
    else
    {
      vertex.textCoord.x = vertex.textCoord.y = 0.0f;
    }

    vertices.push_back(vertex);

  }

  for (size_t f = 0; f < t_mesh->mNumFaces; ++f)
  {
    aiFace face = t_mesh->mFaces[f];
    for (size_t i = 0; i < face.mNumIndices; ++i)
    {
      indices.push_back(face.mIndices[i]);
    }
  }

  Mesh* mesh = new Mesh(vertices, indices);
  m_meshes.push_back(Mesh::AddMesh(mesh));

  AABBMax.x = AABBMax.x < t_mesh->mAABB.mMax.x ? t_mesh->mAABB.mMax.x : AABBMax.x;
  AABBMax.y = AABBMax.y < t_mesh->mAABB.mMax.y ? t_mesh->mAABB.mMax.y : AABBMax.y;
  AABBMax.z = AABBMax.z < t_mesh->mAABB.mMax.z ? t_mesh->mAABB.mMax.z : AABBMax.z;
  AABBMin.x = AABBMin.x > t_mesh->mAABB.mMin.x ? t_mesh->mAABB.mMin.x : AABBMin.x;
  AABBMin.y = AABBMin.y > t_mesh->mAABB.mMin.y ? t_mesh->mAABB.mMin.y : AABBMin.y;
  AABBMin.z = AABBMin.z > t_mesh->mAABB.mMin.z ? t_mesh->mAABB.mMin.z : AABBMin.z;

}

void Model::Update(float deltaTime) {}

void Model::Draw(mat4 t_model_matrix)
{
  mat4 model = Actor::GetActorByID(m_parent_id)->m_transform.GetModelMatrix();
  for (MeshIt it = m_meshes.begin(); it != m_meshes.end(); ++it)
  {
    Mesh::GetMeshByID(*it)->Draw(m_shader, model);
  }
}
